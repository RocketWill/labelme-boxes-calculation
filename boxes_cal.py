from argparse import ArgumentParser
import json
import os
from typing import List, Dict
from collections import defaultdict

def zero():
    return 0

def loadJsonFiles(dir_path: str) -> List[str]:
    if os.path.isdir(dir_path):
        try:
            return [os.path.join(dir_path, f) for f in os.listdir(dir_path) if
                          os.path.join(dir_path, f).endswith('.json')]
        except:
            raise ValueError('Could not fetch json files in the path.')
    else:
        raise ValueError('Please enter a valid path.')

def calculate_boxes_single_file(file_path: str) -> None:
    global boxes_info
    try:
        with open(file_path, 'r') as reader:
            jf = json.loads(reader.read())
            shapes = jf['shapes']
            if len(shapes) < 1:
                return

            for i in range(len(shapes)):
                label = shapes[i]['label']
                shape_type = shapes[i]['shape_type']
                boxes_info['label'][label] += 1
                boxes_info['shape_type'][shape_type] += 1
    except:
        pass

def write2file(imgs_nums: int, boxes_info: Dict, dst_path: str):
    with open(dst_path, "w") as f:
        f.write("Total Images: " + str(imgs_nums) + '\n')
        f.write("----------------------------------------------------\n")
        f.write("By labels:\n-\n")
        for k, v in boxes_info['label'].items():
            f.write(k + ': '+ str(v) + '\n')
        f.write("\n-\ntotal: " + str(sum(boxes_info['label'].values())) + '\n')
        f.write("----------------------------------------------------\n")
        f.write("By shape types:\n-\n")
        for k, v in boxes_info['shape_type'].items():
            f.write(k + ': ' + str(v) + '\n')
        f.write("\n-\ntotal: " + str(sum(boxes_info['shape_type'].values())) + '\n')

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-src", "--source-folder", dest="src", required=True, help="path to source file")
    parser.add_argument("-o", "--output", dest="output", required=False,
                        help="file output path, default is \"./cal_[source_folder].txt\"")

    args = parser.parse_args()
    src_folder = os.path.abspath(args.src)

    # check source path is exist
    if not os.path.exists(os.path.abspath(src_folder)):
        raise ValueError("Source path does not exist.")

    if not args.output:
        base = os.path.basename(src_folder)
        dst_path = os.path.abspath("./cal_" + base + ".txt")
    else:
        dst_path = os.path.abspath(args.output)

    boxes_info = {'shape_type': defaultdict(zero), 'label': defaultdict(zero)}

    json_files = loadJsonFiles(src_folder)
    for jf in json_files:
        calculate_boxes_single_file(jf)

    write2file(len(json_files), boxes_info, dst_path)
