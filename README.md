### 计算矩形框总数

#### Help
```python
python boxes_cul.py -h

usage: boxes_cul.py [-h] -src SRC [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -src SRC, --source-folder SRC
                        path to source file
  -o OUTPUT, --output OUTPUT
                        file output path, default is
                        "./cal_[source_folder].txt"
```

#### Usage
```python
python boxes_cal.py -src /path/to/json/files/folder -o /path/to/output
```